『羅英辞典』JIS X4081形式(EPWING)データ

北野雅弘氏のホームページ 「北野研究室」( http://www.page.sannet.ne.jp/kitanom/ ) で
配布されているフリーの「Jamming3対応ラテン語辞書」を、EBStudioでJIS X4081形式
(EPWING)に変換した辞書データです。変換スクリプトの作者はrw氏です。

●JIS X 4081(EPWING)羅英辞典

	CATALOGS		CATALOGSファイル
	LATINDIC/DATA/HONMON	JIS X 4081 辞書本文
		/GAIJI/GAI16F00	全角外字
		/GAIJI/GAI16H00	半角外字

●EBStudio用HTMLソース
	HTML/
		latin.html	Jamming3対応ラテン語辞書(正誤表により訂正済み)をEBStudio用HTMLに変換したデータ
		Latindic.ebs	EBStudio用変換定義ファイル
		Gaiji.xml	外字ファイル
		GaijiMap.xml	外字定義ファイル

●Jamming3対応ラテン語辞書からの変換スクリプト

	Latindictk/
		henkan1.pl	perlスクリプト step.1
		henkan2.pl	perlスクリプト step.2
		latindic.bat	変換バッチファイル
		README.txt	README
		ラテン語辞書正誤表.txt	正誤表
		ラテン語辞書.txt	正誤表により訂正済み
		Jamming3用ラテン語辞書オリジナル.txt(北野雅弘氏配布)
===========================================================
開始時刻：2008/10/29 00:43:03
[羅英辞典]
  本文…
  (1)C:¥Latindictk¥latin.html
  done
  図版総数=(0)個
  音声総数=(0)個
  前方一致表記インデックス…(36750)個
  後方一致表記インデックス…(36750)個
  クロス条件検索インデックス…(34304)個
  条件検索インデックス…(134589)個
  外字(8x16)…(282)個 done
  外字(16x16)…(6)個 done
  アンカー総数…(0)個
  リンク総数…(0)個
  C:¥Latindic¥LATINDIC¥DATA¥HONMON ( 16316416 bytes )
終了時刻：2008/10/29 00:43:41
-------------------------
カタログを作成しました。
処理を完了しました。
===========================================================

履歴
2008.10.28	辞書の誤りを若干訂正(正誤表.txt参照)。
表記インデックスが無用に大きくなっているので、不必要な表記インデックスの指定を
とり去るように henkan1.plとhenkan2.plを修正。その結果、辞書のサイズが四分の三
ほどに縮小。また<b>〜</b>タグを使って見出し語を太字で表示するように変更。(rw)

2008.10.08	条件検索インデックスの漏れを修正(rw)

2007.10.28	変換済データを公開(H.Ishida)
