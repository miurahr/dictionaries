『万葉集』JIS X4081形式(EPWING)

□『万葉集』JIS X4081形式(EPWING)について

オンラインで公開されている『万葉集』のフリーテキストを、EBStudioを用いて
JIS X4081形式(EPWING)に変換した辞書データです。
DDWin,EBWin,EBPocketなどのEPWING検索ビューアソフトで閲覧することができます。

(1) 万葉集 〜底本 西本願寺本〜（山口大学 吉村 誠氏）
http://yoshi01.kokugo.edu.yamaguchi-u.ac.jp/makoto/

(2)訓読万葉集 〜鹿持雅澄『萬葉集古義』〜
http://www.asahi-net.or.jp/‾sg2h-ymst/manyok/manyo_k.html


□著作権・配布条件
(1)西本願寺本
著作権は、校訂者である山口大学 吉村 誠氏にあります。ただしGPLとして配布されているため、変換したJIS X4081形式辞書の配布・再利用の条件は、 GPLに準じます。 
(2)鹿持雅澄『萬葉集古義』 フリーテキストとして配布されています。
(3)変換スクリプト フリーウェア

□特徴
(1)西本願寺本
・歌番号による検索。
・仮名による前方一致・後方一致検索。全句の検索が可能。
・表記による前方一致・後方一致検索。長歌は全句での検索が可能。
　短歌はオリジナルテキストが区切られていないため、歌全体の前方一致・後方一致。
・キーワードによる条件検索。
・部立、作者、関連人物、地名、事項による複合検索
・外字に対応

(2)鹿持雅澄『萬葉集古義』
・歌番号による検索。

□使用方法
・配布ファイルを、ディレクトリ構造を保ったままハードディスクに複写します。
・DDWin等のEPWINGビューアで使用します。

□配布ファイル(検索には*のファイルだけが必要です)

CATALOGS		EPWINGカタログファイル(*)
manyo/			【西本願寺本】
    DATA/HONMON		EPWINGデータファイル(*)
    GAIJI/GAI16F00	EPWING外字ファイル(*)

manyok/			【鹿持雅澄『萬葉集古義』】
    DATA/HONMON		EPWINGデータファイル(*)
    GAIJI/


script/			変換スクリプト
README.txt		このファイル

□画像について
・一部の有名歌について、実験的にイメージ画像を収録しています。
下記サイトのフリー素材を、配布規約に基づき利用しています。
http://www.photolibrary.jp/

□履歴
2008/08/23	V2.02	一部の歌にイメージ画像を付加
2008/08/02	V2.01	鹿持雅澄『萬葉集古義』追加
			万葉集テキスト Ver5.00 R1.3 に対応
2005/02/05	V0.20	変換済みデータ公開(西本願寺本)


http://www31.ocn.ne.jp/‾h_ishida/ 

