# dictionaries
Free (as in free beer) EPWING dictionaries

Here is a collection of EPWING dictionaries. Each data are licensed under each product licenses.
Many data is NOT free (as in freedom).

## EDICT2

EDICT2 (EPWING)   Ver. 0.5   2013.05.08

Copyright and license:
Electronic Dictionary Research and Development Group at Monash University
http://www.csse.monash.edu.au/groups/edrdg/

The copyright of this SOFTWARE belongs to the copyright holder of the original data, and licenced under the terms and conditons. This SOFTWARE is a freeware, and permission to copy, modify, and redistribute for any purpose without fee or royalty, and to use for non-commercial is hereby granted.

## GKS02

JIS補助漢字16　　　　2005.08.01
(JIS X 0212-1990)　　Ver. 0.2
Code of the supplementary Japanese graphic character set for information interchange

### Copyright and license:

本ソフトウェア(JIS X 4081 形式)の著作権は、元データの作成者に帰属します。また、本ソフトウェアは、自由に使用し、改編・再配布・再転載可能（但し、制限の有る場合*2を除く）ですが、無償で且つ非営利目的の場合に限ります。

## manyo202

『万葉集』JIS X4081形式(EPWING)

### Copyright and license:

(1)西本願寺本
著作権は、校訂者である山口大学 吉村 誠氏にあります。ただしGPLとして配布されているため、変換したJIS X4081形式辞書の配布・再利用
の条件は、 GPLに準じます。 
(2)鹿持雅澄『萬葉集古義』 フリーテキストとして配布されています。
(3)変換スクリプト フリーウェア

## mhdb01

M's 歴史DB (EPWING)
2004.09.24　　Ver. 0.1

### copyright and license:

原典「歴史データベース」の「著作権について」、及び「データの転載について」の内容が、全く同様に適用される。
このソフトウェア(JIS X 4081 形式)は、上記事項を同様に遵守する限りにおいて、無償で、且つ非営利目的にのみ、自由に使用し、また、再配布・再転載可能である。

## mthes01

Moby Thesaurus (EPWING)    Ver. 0.1     2005.09.01

### copyright and license

The original data is public domain. This SOFTWARE in the format of JIS X 4081 is a freeware, and permission to copy, modify and redistribute for any purpose without fee or royalty, and to use for non-commercial is hereby granted.


## pddj01

「私立ＰＤＤ図書館」　　　　　　　　　　2004.02.14
「人名辞典」EPWING版　　　　　　　　　　Ver. 0.11

### copyright and license

Maximilk

　　(1) 原文テキスト・画像データ(パブリックドメイン)に準じ、本ソフトウェアー(電子辞書)も基本的にパブリックドメインとし、改変・転載・再配布等に関しては自由であるが、｢私立ＰＤＤ図書館｣(｢ＰＤＤ図書館からのお知らせ｣)の趣旨に準ずるものとする。(市販雑誌の付録のＣＤへの収録等は良いが、営利を目的に転載や配布はできない。)

　　(2) EBStudioで作成された本ソフトウェアー(電子辞書)は、「EBStudioで作成された電子辞書の配布規定」に準じ、自由に配布できる。(個人間の売買、雑誌付録等への収録を含む。）

　　(3) 上記(1)及び(2)の理由により、営利・商用(販売)目的での利用はできない。

## w191302

Webster's 1913 (EPWING)	Ver. 0.2	2004.07.24

### Copyright & License

The original texts, both printed and electric versions, are public domain.  This SOFTWARE, based on such texts and converted into the format JIS X 4081-1996, is a copyrighted freeware, and permission to copy, modify and redistribute for any purpose without fee or royalty, and for non-commercial use is hereby granted.

## wdkjt01

和独辞典 WaDokuJT (EPWING)　　　Ver. 0.1
Japanisch-Deutsches Wörterbuch　　　 2006.01.11

### Copyright & license

本ソフトウェアー(JIS X 4081 形式)の著作権は、元データの著作者に帰属する。また、本ソフトウェアーは、自由に使用し、改編・再配布・再転載等が可能であるが、無償で且つ非営利目的の場合に限る。

## 15tup

Fifteen Thousand Useful Phrases (EPWING)
Ver. 0.1   2007.11.11

### Copyright & license

LICENSE of the Project Gutenberg.

## cebd01


漢英佛學大辭典 (EPWING)   Ver. 0.1   2006.09.12
A Dictionary of Chinese Buddhist Terms
   compiled by
William Edward Soothill and Lewis Hodous


### Copyright & license

電子佛教辭典 Digital Dictionary of Buddhism (DDB)
Attribution-NonCommercial-ShareAlike 1.0

efont: The Electronic Font Open Laboratory.

The copyright of this SOFTWARE, in the format of the JIS X 4081, is attributable and belongs to the copyright holders of the original data. This software is a freeware, and the permission to copy, modify and redistribute for any purpose without fee or royalty, and for non-commercial use, provided that such use complies with the original license conditions, is hereby granted.

## jbible107

口語訳聖書(新約1954年版、旧約1955年版) JIS X4081(EPWING)

### Copyright & license

著作権フリー

口語訳新約聖書(1954年版)、口語訳旧約聖書(1955版)
大正改訳聖書(1917年版)
明治元訳「舊約聖書」(1887年版)
ラゲ訳 我主イエズスキリストの新約聖書(1910年版)
電網聖書

## latindic102

『羅英辞典』JIS X4081形式(EPWING)データ


### Copyright & license

北野雅弘氏のホームページ 「北野研究室」( http://www.page.sannet.ne.jp/kitanom/ ) で 配布されているフリーの「Jamming3対応ラテン語辞書」 を、EBStudioでJIS X4081形式 (EPWING)に変換した辞書データです。

## cockt01

カクテル・レシピ集 (EPWING版)　Ver. 0.1
Cocktail Recipes　　　　　　　　 2005.04.01

### Copyright & license

(1)カクテル・レシピ集
　　Desktop Drinks Data (2002/12/31版, ddd021231.lzh)
　　制作＆配給: K's Space
　　http://home4.highway.ne.jp/KsSpace/

(2)カクテル・アイコン集
　　カクテルアイコン 2.0 (coc.lzh, 1999.11.21)
　　作者: アイコンCustomMade／はれぶた
　　http://www.vector.co.jp/soft/win95/amuse/se124888.html

本ソフトウェアー(JIS X 4081 形式)の著作権は、元データの著作者に帰属します。また、本ソフトウェアーは、無償・非営利目的の場合に限り自由に使用可能ですが、配布・転載の場合は、基本的に、元データの配布規定に従うものとします。

